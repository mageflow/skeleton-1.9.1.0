#!/bin/sh
if [ -z $1 ]; then echo "Usage: $0 version"; exit 1; fi
./mage uninstall community MageFlowConnector
./mage install-file /Users/sven/Downloads/MageFlowConnector-$1.tgz
git add app/code/community/Mageflow
git add lib
git add shell
git ci -m "Upgraded MFx to $1"
git pull 
git push
echo "Upgrade done"
