<?php

/**
 * MageFlow
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@mageflow.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * If you wish to use the MageFlow Connect extension as part of a paid
 * service please contact licence@mageflow.com for information about
 * obtaining an appropriate licence.
 */

/**
 * Tabs.php
 *
 * PHP version 5
 *
 * @category   MFX
 * @package    Mageflow_Connect
 * @subpackage Block
 * @author     MageFlow OÜ, Estonia <info@mageflow.com>
 * @copyright  Copyright (C) 2014 MageFlow OÜ, Estonia (http://mageflow.com) 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://mageflow.com/
 */

/**
 * Mageflow_Connect_Block_Adminhtml_Migrate_Grid
 *
 * @category   MFX
 * @package    Mageflow_Connect
 * @subpackage Block
 * @author     MageFlow OÜ, Estonia <info@mageflow.com>
 * @copyright  Copyright (C) 2014 MageFlow OÜ, Estonia (http://mageflow.com) 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://mageflow.com/
 */
class Mageflow_Connect_Block_Adminhtml_Push_Tabs extends Mage_Adminhtml_Block_Widget_Tabs {

    /**
     * Class constructor
     */
    public function __construct() {
        parent::__construct();
        $this->setId('push_tabs');
    }

    /**
     * function to return correct version of this array
     * to be extended in different mfx versions
     * 
     * @return type
     */
    protected function getTypeMap() {
        return Mage::getModel('mageflow_connect/system_config')->getTypeMap();
    }
    
    /**
     * create tabs by the list of tabs
     * the official list shall be reviewed
     * 
     * @return type
     */
    protected function _prepareLayout() {

        $typeMap = $this->getTypeMap();

        foreach ($typeMap as $key => $type) {
            if (Mage::app()->getStore()->getConfig('mageflow_connect/enabled_types/' . $type['config'])) {
                $this->addTab($key, array(
                    'label' => $type['label'],
                    'url' => $this->getUrl('*/*/' . $key, array('_current' => true))
                    //'class' => 'ajax'
                ));
            }
        }

        return parent::_prepareLayout();
    }

    /**
     * Translate html content
     *
     * @param string $html
     * @return string
     */
    protected function _translateHtml($html) {
        Mage::getSingleton('core/translate_inline')->processResponseBody($html);
        return $html;
    }

}
