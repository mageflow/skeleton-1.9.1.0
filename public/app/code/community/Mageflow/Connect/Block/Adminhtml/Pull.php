<?php

/**
 * MageFlow
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@mageflow.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * If you wish to use the MageFlow Connect extension as part of a paid
 * service please contact licence@mageflow.com for information about
 * obtaining an appropriate licence.
 */

/**
 * Pull.php
 *
 * PHP version 5
 *
 * @category   MFX
 * @package    Mageflow_Connect
 * @subpackage Block
 * @author     MageFlow OÜ, Estonia <info@mageflow.com>
 * @copyright  Copyright (C) 2014 MageFlow OÜ, Estonia (http://mageflow.com) 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://mageflow.com/
 */

/**
 * Mageflow_Connect_Block_Adminhtml_Pull
 *
 * @category   MFX
 * @package    Mageflow_Connect
 * @subpackage Block
 * @author     MageFlow OÜ, Estonia <info@mageflow.com>
 * @copyright  Copyright (C) 2014 MageFlow OÜ, Estonia (http://mageflow.com) 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://mageflow.com/
 */
class Mageflow_Connect_Block_Adminhtml_Pull
    extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * Class constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->removeButton('add');
        /**
         * @var Mageflow_Connect_Model_Changeset_Item_Cache $model
         */
        $model = Mage::getModel('mageflow_connect/changeset_item_cache');
        $this->addButton('refresh', array(
            'label' => Mage::helper('mageflow_connect')->__('Refresh Remote Change Items'),
            'before_html' => $this->getBeforeHtml($model),
            'onclick' => "window.location = '" . $this->getUrl('*/*/refreshpullgridpage') . "';"
        ));
        $this->_controller = 'adminhtml_pull';
        $this->_blockGroup = 'mageflow_connect';
        $this->_headerText = Mage::helper('mageflow_connect')->__(
            'Remote Change Items'
        );

    }

    /**
     * Returns html before refresh button
     *
     * @param $model
     * @return string
     */
    protected function getBeforeHtml($model)
    {
        $groundRulesUrl = Mage::app()->getStore()->getConfig(Mageflow_Connect_Model_System_Config::GROUND_RULES_URL);
        $html = sprintf(
            '<strong><a href="%s" id="MFGroundRules" target="_blank">%s</a></strong> ',
            $groundRulesUrl,
            Mage::helper('mageflow_connect')->__('NB! Please read the Ground Rules before migration!')
        );
        $html .= 'Index updated at ' . $model->getLastUpdated()->toString(Zend_Date::DATETIME_MEDIUM);
        return $html;
    }
}
