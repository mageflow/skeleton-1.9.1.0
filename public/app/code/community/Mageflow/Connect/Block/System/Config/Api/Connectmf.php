<?php

/**
 * MageFlow
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@mageflow.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * If you wish to use the MageFlow Connect extension as part of a paid
 * service please contact licence@mageflow.com for information about
 * obtaining an appropriate licence.
 */

/**
 * Connectmf.php
 *
 * PHP version 5
 *
 * @category   MFX
 * @package    Mageflow_Connect
 * @subpackage Block
 * @author     MageFlow OÜ, Estonia <info@mageflow.com>
 * @copyright  Copyright (C) 2014 MageFlow OÜ, Estonia (http://mageflow.com) 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://mageflow.com/
 */

/**
 * Mageflow_Connect_Block_System_Config_Api_Connectmf
 * Creates "Connect to MageFlow" button
 *
 * @category   MFX
 * @package    Mageflow_Connect
 * @subpackage Block
 * @author     MageFlow OÜ, Estonia <info@mageflow.com>
 * @copyright  Copyright (C) 2014 MageFlow OÜ, Estonia (http://mageflow.com) 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://mageflow.com/
 */
class Mageflow_Connect_Block_System_Config_Api_Connectmf
    extends Mageflow_Connect_Block_System_Config_Api_Basebutton
{
    /**
     * Creates "connect to api" button
     *
     * @param Mage_Core_Block_Abstract $buttonBlock
     *
     * @return string
     */
    public function getButtonData($buttonBlock)
    {
        $data = array(
            'label' => Mage::helper('mageflow_connect')->__(
                    "Connect to MageFlow"
                ),
            'class' => '',
            'comment' => '',
            'id' => 'btn_connect_mf',
            'data-api-url' => Mage::helper("adminhtml")->getUrl(
                    'adminhtml/ajax/gettoken'
                ) . '?isAjax=true',
            'onclick' => 'javascript:;',
            'after_html' => $this->getAfterHtml(),
            'before_html' => $this->getBeforeHtml()
        );
        return $data;
    }

    /**
     * Returns HTML that is prepended to button
     *
     * @return string
     */
    protected function getBeforeHtml()
    {
        $link = $this->getSignupUrl();
        $html
            = <<<HTML
            <p>
            Please <a href="$link" target="_blank">click here to sign up</a> if you don't have an account at MageFlow yet.
            </p>
HTML;

        return $html;
    }

    /**
     * Returns HTML that is appended to button
     *
     * @return string
     */
    protected function getAfterHtml()
    {
        $html = '';
        return $html;
    }

    /**
     * get signup url
     */
    protected function getSignupUrl()
    {
        return Mage::app()->getStore()->getConfig(Mageflow_Connect_Model_System_Config::SIGNUP_URL);
    }

}
