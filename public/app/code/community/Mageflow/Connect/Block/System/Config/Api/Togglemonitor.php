<?php

/**
 * MageFlow
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@mageflow.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * If you wish to use the MageFlow Connect extension as part of a paid
 * service please contact licence@mageflow.com for information about
 * obtaining an appropriate licence.
 */

/**
 * Connectmf.php
 *
 * PHP version 5
 *
 * @category   MFX
 * @package    Mageflow_Connect
 * @subpackage Block
 * @author     MageFlow OÜ, Estonia <info@mageflow.com>
 * @copyright  Copyright (C) 2014 MageFlow OÜ, Estonia (http://mageflow.com) 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://mageflow.com/
 */

/**
 * Mageflow_Connect_Block_System_Config_Api_Connectmf
 * Creates "Connect to MageFlow" button
 *
 * @category   MFX
 * @package    Mageflow_Connect
 * @subpackage Block
 * @author     MageFlow OÜ, Estonia <info@mageflow.com>
 * @copyright  Copyright (C) 2014 MageFlow OÜ, Estonia (http://mageflow.com) 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://mageflow.com/
 */
class Mageflow_Connect_Block_System_Config_Api_Togglemonitor
    extends Mageflow_Connect_Block_System_Config_Api_Basebutton
{
    /**
     * Overloads parent's render in order to make display of button
     * depending on instance connection status (availability of keys)
     *
     * @param Varien_Data_Form_Element_Abstract $element
     * @return string|void
     */
    public function render(Varien_Data_Form_Element_Abstract $element)
    {
        //TODO
        //verify instance connection status here
        $instanceConnected =
            (
                Mage::app()->getStore()->getConfig(Mageflow_Connect_Model_System_Config::API_CONSUMER_KEY) != ''
                && Mage::app()->getStore()->getConfig(Mageflow_Connect_Model_System_Config::API_CONSUMER_SECRET) != ''
                && Mage::app()->getStore()->getConfig(Mageflow_Connect_Model_System_Config::API_TOKEN) != ''
                && Mage::app()->getStore()->getConfig(Mageflow_Connect_Model_System_Config::API_TOKEN_SECRET) != ''
            );
        if (!$instanceConnected) {
            return '';
        } else {
            return parent::render($element);
        }
    }

    /**
     * Creates "connect to api" button
     *
     * @param Mage_Core_Block_Abstract $buttonBlock
     *
     * @return string
     */
    public function getButtonData($buttonBlock)
    {
        $data = array(
            'label' => Mage::helper('mageflow_connect')->__(
                    "Toggle monitoring"
                ),
            'class' => '',
            'comment' => '',
            'id' => 'btn_toggle_monitor',
            'data-api-url' => Mage::helper("adminhtml")->getUrl(
                    'adminhtml/ajax/togglemonitor'
                ) . '?isAjax=true',
            'onclick' => 'javascript:;',
            'after_html' => $this->getAfterHtml(),
            'before_html' => $this->getBeforeHtml()
        );
        return $data;
    }

    /**
     * Returns HTML that is prepended to button
     *
     * @return string
     */
    protected function getBeforeHtml()
    {
        $html
            = <<<HTML
            <p>
            By clicking this button monitoring status can be toggled. MageFlow will not monitor this instance nor push Change Items here
            if monitoring is off.
            </p>
HTML;

        return $html;
    }

    /**
     * Returns HTML that is appended to button
     *
     * @return string
     */
    protected function getAfterHtml()
    {
        $html = '';
        return $html;
    }

}
