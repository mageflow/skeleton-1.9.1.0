<?php

/**
 *
 * Abstract.php
 *
 * @author sven
 * @created 06/27/2014 17:32
 */
class Mageflow_Connect_Model_Abstract extends Varien_Object
{

    /**
     * @param $msg
     */
    protected function log($msg)
    {
        /**
         * @var Mageflow_Connect_Helper_Log $helper
         */
        $helper = Mage::helper('mageflow_connect/log');
        $helper->log($msg);
    }
} 