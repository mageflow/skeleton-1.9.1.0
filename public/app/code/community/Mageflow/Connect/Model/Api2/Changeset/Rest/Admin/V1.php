<?php

/**
 * MageFlow
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@mageflow.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * If you wish to use the MageFlow Connect extension as part of a paid
 * service please contact licence@mageflow.com for information about
 * obtaining an appropriate licence.
 */

/**
 * V1.php
 *
 * PHP version 5
 *
 * @category   MFX
 * @package    Mageflow_Connect
 * @subpackage Model
 * @author     MageFlow OÜ, Estonia <info@mageflow.com>
 * @copyright  Copyright (C) 2014 MageFlow OÜ, Estonia (http://mageflow.com) 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://mageflow.com/
 */

/**
 * Mageflow_Connect_Model_Api2_Changeset_Rest_Admin_V1
 *
 * @category   MFX
 * @package    Mageflow_Connect
 * @subpackage Model
 * @author     MageFlow OÜ, Estonia <info@mageflow.com>
 * @copyright  Copyright (C) 2014 MageFlow OÜ, Estonia (http://mageflow.com) 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://mageflow.com/
 */
class Mageflow_Connect_Model_Api2_Changeset_Rest_Admin_V1
    extends Mageflow_Connect_Model_Api2_Abstract
{
    /**
     * resource type
     *
     * @var string
     */
    protected $_resourceType = 'changeset';


    /**
     * Returns JSON array with changesets
     *
     * A single changeset can be retrieved by mf_guid
     *
     * @return json
     */
    public function _retrieve()
    {
        $out = array();
        $key = $this->getRequest()->getParam('key', null);

        if (stristr($this->getRequest()->getRequestUri(), '/recover/')) {
            return $this->recoverAction($key);
        } else {

            $modelCollection = $this->getWorkingModel()->getCollection();
            if (null !== $key) {
                $modelCollection->addFilter('mf_guid', $key);
            }
            $out = $this->packModelCollection($modelCollection);
        }
        return $out;
    }

    /**
     * This actions responds to recover request and recovers a changesetitem
     * by its mf_guid
     * @param $key
     *
     * @return array|void
     * @throws Exception
     * @throws Mage_Api2_Exception
     */
    protected function recoverAction($key)
    {
        $this->log('Recovering changeset ' . $key);

        /**
         * @var Mageflow_Connect_Model_Changeset_Item $model
         */
        $model = $this->getWorkingModel()->load($key, 'mf_guid');

        if ($model->getId() > 0) {

            $filteredData = json_decode($model->getContent(), true);

            $typeName = str_replace(':', '_', $model->getType());

            $out = $this->getDataProcessor($typeName)->processData($filteredData);

            return array();

        } else {
            return $this->_critical('Requested changeset not found!', 404);
        }
    }

    /**
     * Returns list of admin users.
     *
     * @return array
     */
    public function _retrieveCollection()
    {
        return parent::_retrieveCollection();
    }

    public function _update($key)
    {
        $out = array();
        return $out;
    }

}