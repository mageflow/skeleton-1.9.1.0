<?php
/**
 *
 * Changeitem.php
 *
 * @author sven
 * @created 10/15/2014 15:25 
 */

interface Mageflow_Connect_Model_Interfaces_Changeitem {

    public function getMfGuid();

    public function getContent();
} 