<?php

/**
 *
 * Media.php
 *
 * @author sven
 * @created 10/20/2015 23:07
 */
class Mageflow_Connect_Model_Product_Attribute_Backend_Media extends Mage_Catalog_Model_Product_Attribute_Backend_Media
{
    public function beforeSave($object)
    {
        if ($object->getData('is_mageflow_import')) {
            return $this;
        }
        return parent::beforeSave($object);
    }
}