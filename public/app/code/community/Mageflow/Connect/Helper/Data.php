<?php

/**
 * MageFlow
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@mageflow.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * If you wish to use the MageFlow Connect extension as part of a paid
 * service please contact licence@mageflow.com for information about
 * obtaining an appropriate licence.
 */

/**
 * Data.php
 *
 * PHP version 5
 *
 * @category   MFX
 * @package    Mageflow_Connect
 * @subpackage Helper
 * @author     MageFlow OÜ, Estonia <info@mageflow.com>
 * @copyright  Copyright (C) 2014 MageFlow OÜ, Estonia (http://mageflow.com) 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://mageflow.com/
 */

/**
 * Mageflow_Connect_Helper_Data
 *
 * @category   MFX
 * @package    Mageflow_Connect
 * @subpackage Helper
 * @author     MageFlow OÜ, Estonia <info@mageflow.com>
 * @copyright  Copyright (C) 2014 MageFlow OÜ, Estonia (http://mageflow.com) 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://mageflow.com/
 */
class Mageflow_Connect_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Returns MageFlow setting value from config.xml
     *
     * @param $path
     *
     * @return string
     */
    public function getSettingValue($path)
    {
        $arr = Mage::app()->getConfig()->getXpath($path);
        /**
         * @var Mage_Core_Model_Config_Element $el
         */
        $el = $arr[0];
        $value = null;
        if ($el instanceof Mage_Core_Model_Config_Element) {
            if (null != $el->getAttribute('value')) {
                $value = $el->getAttribute('value');
            } else {
                $value = (string)$el;
            }
        }
        return $value;
    }

    /**
     * Returns hash of pretty random bytes
     *
     * @param int $length
     *
     * @return string
     */
    public function randomHash($length = 32)
    {
        $helper = Mage::helper('core');

        return $helper->getRandomString(
            $length, Mage_Core_Helper_Data::CHARS_DIGITS . Mage_Core_Helper_Data::CHARS_LOWERS
        );
    }

    /**
     * create changesetitem object of type from content
     * type must be with ":", like "cms:block"
     * content must be array from getData()
     *
     * @param stdClass $type
     * @param null     $model
     * @param boolean  $forceValidation
     *
     * @internal param $content
     *
     * @return Mageflow_Connect_Model_Changeset_Item
     */
    public function createChangesetFromItem($type, $model = null, $forceValidation = false)
    {
        $changesetItem = Mage::getModel('mageflow_connect/changeset_item');

        if ($type->short == 'tax/class') {
            $classType = $model->getData('class_type');
            if ($classType == 'CUSTOMER') {
                $type->name = 'sales_tax_class_customer';
            }
            if ($classType == 'PRODUCT') {
                $type->name = 'sales_tax_class_product';
            }
        }

        if ($type->name == 'base_url') {
            $this->log($model->getData('path'));
            if (
                $model->getData('path') == Mage_Core_Model_Store::XML_PATH_SECURE_BASE_URL
                || $model->getData('path') == Mage_Core_Model_Store::XML_PATH_UNSECURE_BASE_URL
            ) {

                $this->updateBaseUrl($model);

                return false;
            }
        }

        $packer = $this->getPacker($type, $model);

        if ($packer instanceof Mageflow_Connect_Model_Interfaces_Dataprocessor) {

            $checkSum = $packer->calculateChecksum($model);

            $duplicateCsItem = $changesetItem->load($checkSum, 'checksum');

            if ($duplicateCsItem->getId() < 1 && ($packer->validate($model) || $forceValidation === true)) {

                /**
                 * @var stdClass $packedModel
                 */
                $packedModel = $packer->packData($model);

                if (null !== $packedModel) {

                    $oldItems =  Mage::getModel('mageflow_connect/changeset_item')
                        ->getCollection()
                        ->addFilter('is_current', true)
                        ->addFilter('item_mf_guid', $packedModel->mf_guid);
                    
                    foreach($oldItems as $oldItem) {
                        $oldItem->setIsCurrent(false);
                        $oldItem->save();
                    }
                    
                    $type = str_replace('_', ':', $type->name);

                    $this->log($type);

                    $mfGuid = $this->randomHash(32);

                    $encodedContent = json_encode(
                        $packedModel,
                        JSON_FORCE_OBJECT
                    );

                    $metaInfo = $this->createChangeSetItemMetaInfo($encodedContent, $mfGuid, $model->getMfGuid());

                    $now = new Zend_Date();
                    $changesetItem->setContent($encodedContent);
                    $changesetItem->setType($type);
                    $changesetItem->setEncoding('json');
                    $changesetItem->setCreatedAt($now->toString('c'));
                    $changesetItem->setUpdatedAt($now->toString('c'));
                    $changesetItem->setMetainfo($metaInfo);
                    $changesetItem->setMfGuid($mfGuid);
                    $changesetItem->setChecksum($checkSum);
                    $changesetItem->setItemMfGuid($packedModel->mf_guid);
                    $changesetItem->setIsCurrent(true);

                    $changesetItem->save();
                }
            }
        }

        return $changesetItem;
    }

    /**
     * Fills in changesetitem metainfo
     *
     * @param string $model
     * @param null   $csItemMfGuid
     * @param null   $itemMfGuid
     *
     * @return string
     */
    private function createChangeSetItemMetaInfo($model = null, $csItemMfGuid = null, $itemMfGuid = null)
    {
        $metaInfo = new stdClass();
        $metaInfo->unsecure_base_url = Mage::app()->getStore()->getConfig(
            Mage_Core_Model_Store::XML_PATH_UNSECURE_BASE_URL
        );
        $metaInfo->secure_base_url = Mage::app()->getStore()->getConfig(
            Mage_Core_Model_Store::XML_PATH_SECURE_BASE_URL
        );
        $metaInfo->created_by = $this->getAdminUserName();
        $metaInfo->sha1 = sha1($model);
        $metaInfo->mf_guid = $csItemMfGuid;
        if (null !== $itemMfGuid) {
            $metaInfo->item_mf_guid = $itemMfGuid;
        }
        return json_encode($metaInfo);
    }

    /**
     * Helper method to retrieve admin user name
     *
     * @return string
     */
    public function getAdminUserName()
    {
        if (
            Mage::getSingleton('admin/session')
            && Mage::getSingleton('admin/session')->getUser()
            && Mage::getSingleton('admin/session')->getUser()->getId()
        ) {
            return Mage::getSingleton('admin/session')->getUser()->getUsername();
        }
        return 'n/a';
    }

    /**
     * Puts base URL to MageFlow API
     *
     * @param Mage_Core_Model_Abstract $model
     */
    private function updateBaseUrl($model)
    {
        $this->log('base_url change');

        $company = Mage::app()->getStore()->getConfig(
            Mageflow_Connect_Model_System_Config::API_COMPANY
        );
        $instanceKey = Mage::app()->getStore()
            ->getConfig(
                Mageflow_Connect_Model_System_Config::API_INSTANCE_KEY
            );

        $data = array(
            'command'               => 'change_base_url',
            'instance_key'          => $instanceKey,
            'company'               => $company,
            'web_unsecure_base_url' => $model->getData('value'),
            'web_secure_base_url'   => Mage::app()->getStore()->getConfig(
                Mage_Core_Model_Store::XML_PATH_SECURE_BASE_URL
            ),
        );

        $this->log($data);

        $response = $this->getApiClient()->put('instance/' . $instanceKey, $data);

        $this->log('response ' . print_r($response, true));
    }

    /**
     * Packer factory that helps to get packer for given type
     *
     * @param stdClass $type
     * @param object   $instance
     *
     * @return Mage_Core_Helper_Abstract|null
     */
    public function getPacker($type, $instance = null)
    {
        /**
         * @var Mageflow_Connect_Helper_Type $typeHelper
         */
        $typeHelper = Mage::helper('mageflow_connect/type');
        $handlerClass = $typeHelper->getHandlerClass($type->name, $instance);
        if (class_exists($handlerClass, true)) {
            return Mage::getModel($handlerClass);
        }
        return null;
    }


    /**
     * get changeset collection from mf api
     *
     * @param int $limit number of days since when pull changeset items
     *
     * @return Varien_Data_Collection
     */
    public function getItemCollectionFromMfApi($limit = 60)
    {
        $company = Mage::app()->getStore()->getConfig(
            \Mageflow_Connect_Model_System_Config::API_COMPANY
        );
        $project = Mage::app()->getStore()->getConfig(
            \Mageflow_Connect_Model_System_Config::API_PROJECT
        );
        $data = array(
            'company'    => $company,
            'project'    => $project,
            'limit_days' => $limit
        );

        $client = Mage::helper('mageflow_connect/oauth')->getApiClient();
        $response = $client->get('changeset', $data);
        $changesetDataArr = json_decode($response, true);
        $changesetData = array();
        if (is_array($changesetDataArr)) {
            $changesetData = $changesetDataArr['items'];
        }
        $itemCollection = new Varien_Data_Collection();

        foreach ($changesetData as $changeset) {
            $data['id'] = $changeset['id'];
            $response = $client->get('changeset', $data);
            $changesetItemData = json_decode($response, true);
            foreach (
                $changesetItemData['items'][0]['items'] as $changesetItem
            ) {
                $itemCollection->addItem(
                    new Varien_Object(
                        array(
                            'id'         => $changesetItem['id'],
                            'changeset'  => $changeset['name'],
                            'type'       => $changesetItem['type'],
                            'created_at' => $changesetItem['created_at']
                        )
                    )
                );
            }
        }
        return $itemCollection;
    }

    /**
     * @return \Mageflow\Connect\Model\Api\Mageflow\Client
     */
    protected function getApiClient()
    {
        /**
         * @var Mageflow_Connect_Helper_Oauth $helper
         */
        $helper = Mage::helper('mageflow_connect/oauth');
        $client = $helper->getApiClient();
        return $client;
    }

    /**
     * @param $msg
     *
     * @return mixed
     */
    protected function log($msg)
    {
        $logHelper = Mage::helper('mageflow_connect/log');
        return $logHelper->log($msg);
    }

    /**
     * Returns last access time (frontend OR backend!)
     * of this Magento instance
     */
    public function getLastAccessTime()
    {
        /**
         * @var Mageflow_Connect_Model_Resource_System_Info_Performance_Collection $modelCollection
         */
        $modelCollection = Mage::getModel('mageflow_connect/system_info_performance')->getCollection();
        $modelCollection
            ->setOrder('created_at', 'DESC')
            ->setPageSize(1)
            ->setCurPage(1);

        /**
         * @var Mageflow_Connect_Model_System_Info_Performance $model
         */
        $model = $modelCollection->getFirstItem();

        //output current time if nothing can be found
        $out = time();
        if ($model instanceof Mageflow_Connect_Model_System_Info_Performance) {
            $createdAt = strtotime($model->getCreatedAt());
            $out = $createdAt;
        }
        return $out;
    }

    /**
     * get module version from config
     */
    public function getModuleVersion()
    {
        return Mage::getConfig()->getModuleConfig('Mageflow_Connect')->version;
    }

    /**
     * get module version from config
     */
    public function getModuleName()
    {
        return Mage::getConfig()->getModuleConfig('Mageflow_Connect')->name;
    }

}
